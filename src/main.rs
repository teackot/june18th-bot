use std::{fs::File, io::Read, path::Path};

use roux::{Reddit, Me};
use chrono::prelude::*;
use serde::{Deserialize};

#[derive(Deserialize)]
struct AccountData {
    user_agent: String,
    client_id: String,
    client_secret: String,
    username: String,
    password: String,
}

async fn post_content(bot: &Me, isit: bool, time: &DateTime<Utc>) {
    let mut text = String::new();
    if isit {
        text += "Yes!🎉🎉🎉";
    } else {
        text += format!("#No.\n\nIt is {}", time.date()).as_str();
    };
    text += "\n\n^(I'm a bot and I am written in rust btw)";

    bot.submit_text("Is It June 18th?", text.as_str(), "IsItJune18th")
        .await
        .unwrap();
}

#[tokio::main]
async fn main() {
    // Read account data from config file and deserealize it
    let path: String = std::env::var("HOME").unwrap() + "/.config/june18th.conf";
    let path = Path::new(path.as_str());
    let mut file = match File::open(&path) {
        Err(why) => panic!("Unable to open {}: {}", path.display(), why),
        Ok(file) => file,
    };
    let mut data: String = String::new();
    file.read_to_string(&mut data).expect("Unable to read the config file");
    let account_data: AccountData = toml::from_str(&data).unwrap();

    // Sign in
    let client = Reddit::new(
        &account_data.user_agent,
        &account_data.client_id,
        &account_data.client_secret)
        .username(&account_data.username)
        .password(&account_data.password)
        .login()
        .await;
    
    let bot = client.unwrap();

    // Post the content
    let time = Utc::now();
    post_content(&bot, if time.month() == 6 && time.day() == 18 { true } else { false }, &time).await;
}
